# MINIMAX RISK CLASSIFIERS FOR TIME SERIES CLASSIFICATION

This repository has been created for the development of the Final Degree Dissertation 'Time Series Classification by mean of Minimax Risk Classifiers'. The aim of this FDD is to evaluate which feature mapping performs best when using the Minimax Risk Classifier algorithms for the Time Series Classification approach. For this, the library MRCpy, developed by the Basque Institute of Applied Mathematics (BCAM) has been used (Bondugula, K., Mazuelas, S., & Pérez, A. (2021). MRCpy: A Library for Minimax Risk Classifiers. arXiv preprint arXiv:2108.01952.). 

In the folder 'Datasets' we can find eigth datasets downloaded from the UEA & UCR Time Series Classification Repository: ArrowHead, Beef, BirdChicken, Coffee, Gunpoint, Meat, OliveOil and Wine. 

In the folder 'Tests' we can find the code for the computation of this approach with six feature mappings. Between them, four were already implemented in the library MRCpy: linear, ReLU, Threshold and Fourier. Two more have been developed for this dissertation: RWS and DTW, which are proper of time series. 

